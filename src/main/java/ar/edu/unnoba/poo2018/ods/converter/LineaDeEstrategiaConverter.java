package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.LineaEstrategia;
import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "lineaDeEstrategiaConverter",forClass = LineaEstrategia.class)
public class LineaDeEstrategiaConverter extends AbstractConverter<LineaEstrategia>{

    @Override
    public String getDAOName() {
        return "LineaEstrategiaDAO";
    }
}
