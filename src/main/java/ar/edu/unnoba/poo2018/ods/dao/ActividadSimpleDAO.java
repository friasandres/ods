/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.dao;

import ar.edu.unnoba.poo2018.ods.model.ActividadSimple;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Asus
 */
@Stateless
public class ActividadSimpleDAO extends AbstractDAO<ActividadSimple>{
    
        public ActividadSimpleDAO() {
        super(ActividadSimple.class);
    }

   
   
    public List<ActividadSimple> findAll() {
        Query query = em.createNamedQuery("ActividadSimple.findAll");
        return query.getResultList();
    }

    
}
