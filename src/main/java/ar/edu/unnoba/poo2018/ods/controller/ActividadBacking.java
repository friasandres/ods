package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ActividadDAO;
import ar.edu.unnoba.poo2018.ods.dao.ODSDAO;
import ar.edu.unnoba.poo2018.ods.dao.UsuarioDAO;
import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import ar.edu.unnoba.poo2018.ods.model.Usuario;
import java.io.Serializable;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


@ManagedBean(name = "actividadBacking")
@ViewScoped
public class ActividadBacking implements Serializable {

    private Actividad actividad;
  
     
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
    
    @EJB
    UsuarioDAO usuarioDAO;
    
    @EJB
    ActividadDAO actividadDAO;
    
    @PostConstruct
    public void init() {
       
    }

    public String create() {
        FacesMessage message = null;
         
        try {

          
            getActividadDAO().create(getActividad());
            
            message = new FacesMessage(FacesMessage.FACES_MESSAGES,bundle.getString("objetivoCreado"));
           FacesContext.getCurrentInstance().addMessage(null, message);

            return "/administrador/index?faces-redirect=true";

        } catch (Exception e) {
            message = new FacesMessage(bundle.getString("ocurrioError"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return "/administrador/new?faces-redirect=true";
        }

    }

    public String update() {
        FacesMessage message = null;
         
        try {

            getActividadDAO().update(this.getActividad());
            message = new FacesMessage(bundle.getString("usuarioModificado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return "/administrador/index?faces-redirect=true";
        } catch (Exception e) {
            message = new FacesMessage(bundle.getString("ocurrioError"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }
    }

    public String delete(Usuario s) {
        FacesMessage message = null;
    
        try {

            getUsuarioDAO().delete(s);
            message = new FacesMessage(bundle.getString("usuarioEliminado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return "/administrador/index?faces-redirect=true";
        } catch (Exception e) {
            message = new FacesMessage(bundle.getString("ocurrioError"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }
    }

    public List<Usuario> listaDeUsuarios() {
        return usuarioDAO.findAll();
    }
    
    public List<Actividad> listaDeActividades(){
        return getActividadDAO().findAll();
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }
    
    

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public ActividadDAO getActividadDAO() {
        return actividadDAO;
    }

    public void setActividadDAO(ActividadDAO actividadDAO) {
        this.actividadDAO = actividadDAO;
    }
    

    public ResourceBundle getBundle() {
        return bundle;
    }

    public void setBundle(ResourceBundle bundle) {
        this.bundle = bundle;
    }

    
}
