package ar.edu.unnoba.poo2018.ods.dao;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

@Stateless
public class ODSDAO extends AbstractDAO<ODS> {

    public ODSDAO() {
        super(ODS.class);
    }
    public int pesoTotal() {
        Query query = em.createNamedQuery("ODS.pesoTotal");
        return query.getFirstResult();
    }
   
   
    public List<ODS> findAll() {
        Query query = em.createNamedQuery("ODS.findAll");
        return query.getResultList();
    }

}
