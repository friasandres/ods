package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.Usuario;
import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "usuarioConverter",forClass = Usuario.class)
public class usuarioConverter extends AbstractConverter<Usuario>{

    @Override
    public String getDAOName() {
        return "UsuarioDAO";
    }
}
