package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ActividadSimpleDAO;
import ar.edu.unnoba.poo2018.ods.dao.AmbitoDeEjecucionDAO;
import ar.edu.unnoba.poo2018.ods.dao.ConvocatoriaDAO;
import ar.edu.unnoba.poo2018.ods.dao.ImpactoObjetivoDAO;
import ar.edu.unnoba.poo2018.ods.dao.LineaEstrategiaDAO;
import ar.edu.unnoba.poo2018.ods.dao.ODSDAO;
import ar.edu.unnoba.poo2018.ods.model.ActividadSimple;
import ar.edu.unnoba.poo2018.ods.model.AmbitoDeEjecucion;
import ar.edu.unnoba.poo2018.ods.model.Convocatoria;
import ar.edu.unnoba.poo2018.ods.model.ImpactoObjetivo;
import ar.edu.unnoba.poo2018.ods.model.LineaEstrategia;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import ar.edu.unnoba.poo2018.ods.model.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@ManagedBean(name = "actividadSimpleBacking", eager = true)
@ViewScoped
public class ActividadSimpleBacking implements Serializable {

    private ActividadSimple actividadSimple;
    private ImpactoObjetivo impactoObjetivo;

    @PostConstruct
    public void init() {
        this.actividadSimple = new ActividadSimple();

        this.impactoObjetivo = new ImpactoObjetivo();

    }

    @EJB
    ActividadSimpleDAO actividadSimpleDAO;

    @EJB
    ConvocatoriaDAO convocatoriaDAO;

    @EJB
    LineaEstrategiaDAO lineaEstrategiaDAO;

    @EJB
    AmbitoDeEjecucionDAO ambitoDeEjecucionDAO;

    @EJB
    ImpactoObjetivoDAO impactoObjetivoDAO;

    @EJB
    ODSDAO odsDAO;

    public String create() {
        FacesMessage message = null;
        try {
            
            getActividadSimpleDAO().create(getActividadSimple());
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadCreada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/actividad/simple/index?faces-redirect=true";

        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/simple/index?faces-redirect=true";
        }

    }

    public String update() {
        FacesMessage message = null;
        try {

            getActividadSimpleDAO().update(this.getActividadSimple());

             ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadModificada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
     
            return "/actividad/simple/index?faces-redirect=true";
        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/simple/index?faces-redirect=true";
        }
    }

    public String delete(ActividadSimple s) {
        FacesMessage message = null;

        try {

            getActividadSimpleDAO().delete(s);
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadEliminada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/simple/index?faces-redirect=true";
        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            return "/actividad/simple/index?faces-redirect=true";
        }
    }

    public void borrarImpacto(ImpactoObjetivo impacto) {
        this.getActividadSimple().getImpactoObjetivo().remove(impacto);
        this.getImpactoObjetivoDAO().delete(impacto);
    }

    public void insertarImpacto() {

        getImpactoObjetivo().setActividad(actividadSimple);
        if (actividadSimple.getImpactoObjetivo() == null) {
            actividadSimple.setImpactoObjetivo(new LinkedList<ImpactoObjetivo>());
        }
        this.actividadSimple.getImpactoObjetivo().add(getImpactoObjetivo());
        setImpactoObjetivo(new ImpactoObjetivo());

    }

    public void deshacer() {
        actividadSimple.getImpactoObjetivo().removeLast();
    }

    public boolean impactosVacio() {
        if (actividadSimple.getImpactoObjetivo() == null) {
            return true;
        }
        if (actividadSimple.getImpactoObjetivo().isEmpty()) {
            return true;
        }
        return false;
    }

    public void borrar() {
        this.init();
    }

    public List<ActividadSimple> findAll() {
        return actividadSimpleDAO.findAll();
    }

    public List<ODS> listaDeObjetivos() {
        return odsDAO.findAll();
    }

    public List<Convocatoria> listaDeConvocatorias() {
        return convocatoriaDAO.findAll();
    }

    public List<LineaEstrategia> listaDeEstrategias() {
        return lineaEstrategiaDAO.findAll();
    }

    public List<AmbitoDeEjecucion> listaDeAmbitos() {
        return ambitoDeEjecucionDAO.findAll();
    }

    public ActividadSimple getActividadSimple() {
        return actividadSimple;
    }

    public void setActividadSimple(ActividadSimple actividadSimple) {
        this.actividadSimple = actividadSimple;
    }

    public ActividadSimpleDAO getActividadSimpleDAO() {
        return actividadSimpleDAO;
    }

    public void setActividadSimpleDAO(ActividadSimpleDAO actividadSimpleDAO) {
        this.actividadSimpleDAO = actividadSimpleDAO;
    }

    public ConvocatoriaDAO getConvocatoriaDAO() {
        return convocatoriaDAO;
    }

    public void setConvocatoriaDAO(ConvocatoriaDAO convocatoriaDAO) {
        this.convocatoriaDAO = convocatoriaDAO;
    }

    public LineaEstrategiaDAO getLineaEstrategiaDAO() {
        return lineaEstrategiaDAO;
    }

    public void setLineaEstrategiaDAO(LineaEstrategiaDAO lineaEstrategiaDAO) {
        this.lineaEstrategiaDAO = lineaEstrategiaDAO;
    }

    public AmbitoDeEjecucionDAO getAmbitoDeEjecucionDAO() {
        return ambitoDeEjecucionDAO;
    }

    public void setAmbitoDeEjecucionDAO(AmbitoDeEjecucionDAO ambitoDeEjecucionDAO) {
        this.ambitoDeEjecucionDAO = ambitoDeEjecucionDAO;
    }

    public ODSDAO getOdsDAO() {
        return odsDAO;
    }

    public void setOdsDAO(ODSDAO odsDAO) {
        this.odsDAO = odsDAO;
    }

    public ImpactoObjetivo getImpactoObjetivo() {
        return impactoObjetivo;
    }

    public void setImpactoObjetivo(ImpactoObjetivo impactoObjetivo) {
        this.impactoObjetivo = impactoObjetivo;
    }

    public ImpactoObjetivoDAO getImpactoObjetivoDAO() {
        return impactoObjetivoDAO;
    }

    public void setImpactoObjetivoDAO(ImpactoObjetivoDAO impactoObjetivoDAO) {
        this.impactoObjetivoDAO = impactoObjetivoDAO;
    }

    public List<ODS> getObjetivos() {
        return getOdsDAO().findAll();
    }

}
