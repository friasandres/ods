/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Asus
 */
@Entity
@Table(name="ambito")
@NamedQueries({
    @NamedQuery(name = "AmbitoDeEjecucion.findAll", query = "SELECT u FROM AmbitoDeEjecucion u")
})
public class AmbitoDeEjecucion extends AbstractEntity{
    @Column(name="nombre")
    private String nombre;
   
    public AmbitoDeEjecucion() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    
}
