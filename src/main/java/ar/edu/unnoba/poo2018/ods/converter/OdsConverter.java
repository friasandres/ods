package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.ODS;
import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "odsConverter",forClass = ODS.class)
public class OdsConverter extends AbstractConverter<ODS>{

    @Override
    public String getDAOName() {
        return "ODSDAO";
    }
}
