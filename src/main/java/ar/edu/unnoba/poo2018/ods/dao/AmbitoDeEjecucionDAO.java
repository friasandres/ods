/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.dao;

import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.AmbitoDeEjecucion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Asus
 */
@Stateless
public class AmbitoDeEjecucionDAO extends AbstractDAO<AmbitoDeEjecucion>{
        public AmbitoDeEjecucionDAO() {
        super(AmbitoDeEjecucion.class);
    }

   
   
    public List<AmbitoDeEjecucion> findAll() {
        Query query = em.createNamedQuery("AmbitoDeEjecucion.findAll");
        return query.getResultList();
    }

    
}
