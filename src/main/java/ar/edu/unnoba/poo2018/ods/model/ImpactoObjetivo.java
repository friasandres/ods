/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@XmlRootElement
@Table(name="impacto")
@NamedQueries({
    @NamedQuery(name = "ImpactoObjetivo.findAll", query = "SELECT u FROM ImpactoObjetivo u"),
    @NamedQuery(name = "ImpactoObjetivo.totalObjetivo", query = "SELECT i.ods, SUM(i.peso) FROM ImpactoObjetivo i GROUP BY i.ods"),
    @NamedQuery(name = "ImpactoObjetivo.pesoTotal", query = "SELECT SUM(c.peso) FROM ImpactoObjetivo c")
})
public class ImpactoObjetivo extends AbstractEntity{
    @Column(name="peso")
    private int peso;
    
    @JoinColumn(name ="ods_id", referencedColumnName = "id")
    @ManyToOne (optional = false)
    private ODS ods;
    
    @JoinColumn(name ="actividad_id", referencedColumnName = "id")
    @ManyToOne (optional = false)
    private ActividadSimple actividad;
    
    public ImpactoObjetivo() {
    }

   

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public ODS getOds() {
        return ods;
    }

    public void setOds(ODS ods) {
        this.ods = ods;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(ActividadSimple actividad) {
        this.actividad = actividad;
    }
    
}
