package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ActividadDAO;
import ar.edu.unnoba.poo2018.ods.dao.ODSDAO;
import ar.edu.unnoba.poo2018.ods.dao.UsuarioDAO;
import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import ar.edu.unnoba.poo2018.ods.model.Usuario;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


@ManagedBean(name = "usuariosBacking")
@SessionScoped
public class UsuariosBacking implements Serializable {

    private Usuario usuario;
    private List<Actividad> actividades;
    private Actividad actividad;
     
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
    
    @EJB
    UsuarioDAO usuarioDAO;
    
    @EJB
    ActividadDAO actividadDAO;
    
    @PostConstruct
    public void init() {
        this.usuario = new Usuario();

    }
    
    public String actualizar(){
                
        return this.update();
        
    }

    public String create() {
        FacesMessage message = null;
         
        try {

          
            getUsuarioDAO().create(getUsuario());
            
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("usuarioCreado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/administrador/index?faces-redirect=true";

        } catch (EJBException e) {
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/administrador/new?faces-redirect=true";
        }

    }

    public String update() {
        FacesMessage message = null;
         
        try {
            
            getUsuarioDAO().update(this.getUsuario());
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("usuarioModificado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/administrador/index?faces-redirect=true";

        } catch (EJBException e) {
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/administrador/index?faces-redirect=true";
        }
    }

    public String delete(Usuario s) {
        FacesMessage message = null;
    
        try {

            getUsuarioDAO().delete(s);
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("usuarioEliminado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/administrador/index?faces-redirect=true";

        } catch (EJBException e) {
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
           return "/administrador/index?faces-redirect=true";
        }    }
    
     public void insertarActividad() {

        if (usuario.getActividades()==null) {
            usuario.setActividades(new LinkedList<Actividad>());
        }
            this.usuario.getActividades().add(getActividad());
                
    }
    public List<Usuario> findAll() {
        return usuarioDAO.findAll();
    }
    
    public List<Actividad> listaDeActividades(){
        return getActividadDAO().findAll();
    }
    
     public void deshacer() {
        usuario.getActividades().removeLast();
    }

    public boolean actividadesVacio() {
        if (usuario.getActividades() == null) {
            return true;
        }
        if (usuario.getActividades().isEmpty()) {
            return true;
        }
        return false;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    public ActividadDAO getActividadDAO() {
        return actividadDAO;
    }

    public void setActividadDAO(ActividadDAO actividadDAO) {
        this.actividadDAO = actividadDAO;
    }
    

    public ResourceBundle getBundle() {
        return bundle;
    }

    public void setBundle(ResourceBundle bundle) {
        this.bundle = bundle;
    }
    
    public List<Actividad> todasLasActividades(){
        return actividadDAO.findAll();
    }

    public List<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(List<Actividad> actividades) {
        this.actividades = actividades;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }
    
    
    
    
}
