package ar.edu.unnoba.poo2018.ods.dao;
import ar.edu.unnoba.poo2018.ods.model.ImpactoObjetivo;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

@Stateless
public class ImpactoObjetivoDAO extends AbstractDAO<ImpactoObjetivo> {

    public ImpactoObjetivoDAO() {
        super(ImpactoObjetivo.class);
    }

   
   
    public List<ImpactoObjetivo> findAll() {
        Query query = em.createNamedQuery("ImpactoObjetivo.findAll");
        return query.getResultList();
    }
    
    public HashMap<ODS,Double> totalObjetivo() {
        HashMap<ODS,Double> lista = new HashMap<ODS,Double>();
        Query query = em.createNamedQuery("ImpactoObjetivo.totalObjetivo");
        lista= (HashMap<ODS, Double>) query.getResultList();
        return lista;
    }
    public int pesoTotal() {
        Query query = em.createNamedQuery("ImpactoObjetivo.pesoTotal");
        return (int)query.getFirstResult();
        }
}
