/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.dao;

import ar.edu.unnoba.poo2018.ods.model.ActividadCompuesta;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Asus
 */
@Stateless
public class ActividadCompuestaDAO extends AbstractDAO<ActividadCompuesta>{
        public ActividadCompuestaDAO() {
        super(ActividadCompuesta.class);
    }

   
   
    public List<ActividadCompuesta> findAll() {
        Query query = em.createNamedQuery("ActividadCompuesta.findAll");
        return query.getResultList();
    }
    public List<ActividadCompuesta> compuestasConPadre() {
        Query query = em.createNamedQuery("ActividadCompuesta.compuestasConPadre");
       return query.getResultList();
    }


}
