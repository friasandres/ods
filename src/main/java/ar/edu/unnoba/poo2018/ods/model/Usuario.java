/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Asus
 */
@Entity
@Table(name="usuarios")
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.find_mail_and_pw", query = "SELECT u FROM Usuario u WHERE u.mail= :mail and u.password= :password")
     
})
public class Usuario extends AbstractEntity {
    @Column(name="mail")
    private String mail;
    @Column(name="password")
    private String password;
    @Column(name="administrador")
    private boolean administrador;
 @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
    private LinkedList<Lineas> lineas;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "actividad_usuario", schema = "", joinColumns = {
        @JoinColumn(name = "usuario_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "actividad_id", referencedColumnName = "id")})
    private LinkedList<Actividad> actividades;
    public Usuario() {
        
    }
    
    public void asignarActividad(Usuario u, Actividad a){
        u.getActividades().add(a);
    }

    public LinkedList<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(LinkedList<Actividad> actividades) {
        this.actividades = actividades;
    }
    
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean admin) {
        this.administrador = admin;
    }

    public LinkedList<Lineas> getLineas() {
        return lineas;
    }

    public void setLineas(LinkedList<Lineas> lineas) {
        this.lineas = lineas;
    }
    
}
