/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import java.util.LinkedList;
import javax.persistence.OneToMany;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Asus
 */
@Entity
@DiscriminatorValue("Compuesta")
@NamedQueries({
    @NamedQuery(name = "ActividadCompuesta.findAll", query = "SELECT u FROM ActividadCompuesta u"),
    @NamedQuery(name = "ActividadCompuesta.compuestasConPadre", query = "SELECT u FROM ActividadCompuesta u WHERE u.padre is not null")

})
public class ActividadCompuesta extends Actividad {
    @OneToMany
    private LinkedList<Actividad> actividades;
    @ManyToOne
    private Actividad padre;

    public ActividadCompuesta() {
    }
    
    @Override
    public int getPeso() {
        int pesoTotal = 0;
        int cantidad =0;
        for(Actividad i:getActividades()){
            cantidad+=1;
            pesoTotal+=i.getPeso();
        }
        return pesoTotal/cantidad;
    }

    @Override
    public double calcularPorcentaje() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public LinkedList<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(LinkedList<Actividad> actividades) {
        this.actividades = actividades;
    }

    public Actividad getPadre() {
        return padre;
    }

    public void setPadre(Actividad padre) {
        this.padre = padre;
    }
    
    
    
}
