/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Asus
 */
@Entity
@Table(name="ods")
@NamedQueries({
    @NamedQuery(name = "ODS.findAll", query = "SELECT u FROM ODS u"),
     @NamedQuery(name = "ODS.pesoTotal", query = "SELECT SUM(i.pesoTotal)  FROM ODS i ")

    
})
public class ODS extends AbstractEntity{
    @Column(name="nombre")
    private String nombre;
    @Column(name="pesoTotal")
    private int pesoTotal;

    public ODS() {
        
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public double porcentaje(int total, int objetivo){
        
        
        return ((double)objetivo/(double)total)*100;
        
    }

    public double getPesoTotal() {
        return pesoTotal;
    }

    public void setPesoTotal(int pesoTotal) {
        this.pesoTotal = pesoTotal;
    }
    
    
    
}
