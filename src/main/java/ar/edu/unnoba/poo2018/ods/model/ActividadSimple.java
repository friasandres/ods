/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@DiscriminatorValue("Simple")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActividadSimple.findAll", query = "SELECT u FROM ActividadSimple u")
})
public class ActividadSimple extends Actividad {
    @OneToMany(cascade = CascadeType.ALL,mappedBy="actividad")
    private LinkedList<ImpactoObjetivo> impactoObjetivos;
  
    public ActividadSimple() {
    }

    public LinkedList<ImpactoObjetivo> getImpactoObjetivo() {
        return impactoObjetivos;
    }

    public void setImpactoObjetivo(LinkedList<ImpactoObjetivo> impactoObjetivo) {
        this.impactoObjetivos = impactoObjetivo;
    }
    
    @Override
    public int getPeso(){
        int pesoTotal = 0;
        for(ImpactoObjetivo i:getImpactoObjetivo()){
            pesoTotal+=i.getPeso();
        }
        return pesoTotal;
    }    

    @Override
    public double calcularPorcentaje() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
