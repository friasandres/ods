
package ar.edu.unnoba.poo2018.ods.dao;
import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.ActividadCompuesta;
import ar.edu.unnoba.poo2018.ods.model.Usuario;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;


@Stateless
public class ActividadDAO extends AbstractDAO<Actividad> {

    public ActividadDAO() {
        super(Actividad.class);
    }

    
    public List<Actividad> findAll() {
        Query query = em.createNamedQuery("Actividad.findAll");
        return query.getResultList();
    }
    public List<Actividad> actividadesUsuario(Usuario usuario){
        Query query = em.createNamedQuery("Actividad.actividadesUsuario");
        query.setParameter("mail", usuario.getMail());
         List<Actividad> actividades = query.getResultList();
        return actividades;
    }
    public List<Actividad> busqueda(Date desde, Date hasta, Usuario usuario){
        Query query = em.createNamedQuery("Actividad.busqueda");
        query.setParameter("desde", desde);
        query.setParameter("hasta", hasta);       
        query.setParameter("mail", usuario.getMail());         
        List<Actividad> actividades = query.getResultList();
        return actividades;
    }
    
}