package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.Convocatoria;
import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "convocatoriaConverter",forClass = Convocatoria.class)
public class ConvocatoriaConverter extends AbstractConverter<Convocatoria>{

    @Override
    public String getDAOName() {
        return "ConvocatoriaDAO";
    }
}
