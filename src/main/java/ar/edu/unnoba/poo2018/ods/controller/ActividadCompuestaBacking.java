package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ActividadCompuestaDAO;
import ar.edu.unnoba.poo2018.ods.dao.ActividadDAO;
import ar.edu.unnoba.poo2018.ods.dao.ActividadSimpleDAO;
import ar.edu.unnoba.poo2018.ods.dao.AmbitoDeEjecucionDAO;
import ar.edu.unnoba.poo2018.ods.dao.ConvocatoriaDAO;
import ar.edu.unnoba.poo2018.ods.dao.ImpactoObjetivoDAO;
import ar.edu.unnoba.poo2018.ods.dao.LineaEstrategiaDAO;
import ar.edu.unnoba.poo2018.ods.dao.ODSDAO;
import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.ActividadCompuesta;
import ar.edu.unnoba.poo2018.ods.model.ActividadSimple;
import ar.edu.unnoba.poo2018.ods.model.AmbitoDeEjecucion;
import ar.edu.unnoba.poo2018.ods.model.Convocatoria;
import ar.edu.unnoba.poo2018.ods.model.ImpactoObjetivo;
import ar.edu.unnoba.poo2018.ods.model.LineaEstrategia;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@ManagedBean(name = "actividadCompuestaBacking", eager = true)
@ViewScoped
public class ActividadCompuestaBacking implements Serializable {

    private ActividadCompuesta actividadCompuesta;
    private Actividad actividad;

    @PostConstruct
    public void init() {
        this.actividadCompuesta = new ActividadCompuesta();

    }

    @EJB
    ActividadCompuestaDAO actividadCompuestaDAO;

    @EJB
    ActividadSimpleDAO actividadSimpleDAO;

    @EJB
    ConvocatoriaDAO convocatoriaDAO;

    @EJB
    LineaEstrategiaDAO lineaEstrategiaDAO;

    @EJB
    AmbitoDeEjecucionDAO ambitoDeEjecucionDAO;

    @EJB
    ImpactoObjetivoDAO impactoObjetivoDAO;

    @EJB
    ODSDAO odsDAO;

    @EJB
    ActividadDAO actividadDAO;

    public String create() {
        FacesMessage message = null;
        try {
            /**
             * for(ActividadCompuesta
             * a:getActividadCompuesta().getActividades()){
             *
             * }*
             */
            getActividadCompuestaDAO().create(getActividadCompuesta());
            for (ActividadCompuesta a : getActividadCompuestaDAO().findAll()) {
                for (Actividad b : getActividadCompuesta().getActividades()) {
                    if (a.getNombre().equals(b.getNombre())) {
                        a.setPadre(getActividadCompuesta());
                        getActividadCompuestaDAO().update(a);
                    }
                }
            }
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadCreada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/actividad/compuesta/index?faces-redirect=true";

        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/compuesta/index?faces-redirect=true";
        }

    }

    public String update() {
        FacesMessage message = null;
        try {

            getActividadCompuestaDAO().update(getActividadCompuesta());
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadModificada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            for (ActividadCompuesta a : getActividadCompuestaDAO().findAll()) {
                for (Actividad b : getActividadCompuesta().getActividades()) {
                    if (a.getNombre().equals(b.getNombre())) {
                        a.setPadre(getActividadCompuesta());
                        getActividadCompuestaDAO().update(a);
                    }
                }
            }
            return "/actividad/compuesta/index?faces-redirect=true";
        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/compuesta/index?faces-redirect=true";
        }
    }

    public String delete(ActividadCompuesta s) {
        FacesMessage message = null;

        try {

            getActividadCompuestaDAO().delete(s);
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("actividadEliminada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/actividad/compuesta/index?faces-redirect=true";
        } catch (EJBException e) {
            ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/actividad/compuesta/index?faces-redirect=true";
        }
    }

    public void borrar() {
        this.init();
    }

    public void insertarActividad() {

        if (actividadCompuesta.getActividades() == null) {
            actividadCompuesta.setActividades(new LinkedList<Actividad>());
        }
        this.actividadCompuesta.getActividades().add(getActividad());

    }

    public void deshacer() {
        Actividad act = actividadCompuesta.getActividades().getLast();
        for (ActividadCompuesta a : getActividadCompuestaDAO().findAll()) {

            if (a.getNombre().equals(act.getNombre())) {
                a.setPadre(null);
                getActividadCompuestaDAO().update(a);
            }

        }
        actividadCompuesta.getActividades().removeLast();
    }

    public boolean actividadesVacio() {
        if (actividadCompuesta.getActividades() == null) {
            return true;
        }
        if (actividadCompuesta.getActividades().isEmpty()) {
            return true;
        }
        return false;
    }

    public List<ActividadCompuesta> findAll() {
        return getActividadCompuestaDAO().findAll();
    }

    public List<Actividad> listaDeActividadesSinPadre() {
        List<Actividad> lista = listaDeActividades();
        for (Actividad a : listaDeActividades()) {
            for (Actividad b : getActividadCompuestaDAO().compuestasConPadre()) {
                if (a.getNombre().equals(b.getNombre())) {
                    lista.remove(b);
                }
            }
        }
        return lista;
    }

    public List<Actividad> listaDeActividades() {
        return getActividadDAO().findAll();
    }

    public List<ODS> listaDeObjetivos() {
        return odsDAO.findAll();
    }

    public List<Convocatoria> listaDeConvocatorias() {
        return convocatoriaDAO.findAll();
    }

    public List<LineaEstrategia> listaDeEstrategias() {
        return lineaEstrategiaDAO.findAll();
    }

    public List<AmbitoDeEjecucion> listaDeAmbitos() {
        return ambitoDeEjecucionDAO.findAll();
    }

    public ActividadCompuesta getActividadCompuesta() {
        return actividadCompuesta;
    }

    public void setActividadCompuesta(ActividadCompuesta actividadCompuesta) {
        this.actividadCompuesta = actividadCompuesta;
    }

    public ActividadDAO getActividadDAO() {
        return actividadDAO;
    }

    public void setActividadDAO(ActividadDAO actividadDAO) {
        this.actividadDAO = actividadDAO;
    }

    public ConvocatoriaDAO getConvocatoriaDAO() {
        return convocatoriaDAO;
    }

    public void setConvocatoriaDAO(ConvocatoriaDAO convocatoriaDAO) {
        this.convocatoriaDAO = convocatoriaDAO;
    }

    public LineaEstrategiaDAO getLineaEstrategiaDAO() {
        return lineaEstrategiaDAO;
    }

    public void setLineaEstrategiaDAO(LineaEstrategiaDAO lineaEstrategiaDAO) {
        this.lineaEstrategiaDAO = lineaEstrategiaDAO;
    }

    public AmbitoDeEjecucionDAO getAmbitoDeEjecucionDAO() {
        return ambitoDeEjecucionDAO;
    }

    public void setAmbitoDeEjecucionDAO(AmbitoDeEjecucionDAO ambitoDeEjecucionDAO) {
        this.ambitoDeEjecucionDAO = ambitoDeEjecucionDAO;
    }

    public ActividadCompuestaDAO getActividadCompuestaDAO() {
        return actividadCompuestaDAO;
    }

    public void setActividadCompuestaDAO(ActividadCompuestaDAO actividadCompuestaDAO) {
        this.actividadCompuestaDAO = actividadCompuestaDAO;
    }

    public ActividadSimpleDAO getActividadSimpleDAO() {
        return actividadSimpleDAO;
    }

    public void setActividadSimpleDAO(ActividadSimpleDAO actividadSimpleDAO) {
        this.actividadSimpleDAO = actividadSimpleDAO;
    }

    public ODSDAO getOdsDAO() {
        return odsDAO;
    }

    public void setOdsDAO(ODSDAO odsDAO) {
        this.odsDAO = odsDAO;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public ImpactoObjetivoDAO getImpactoObjetivoDAO() {
        return impactoObjetivoDAO;
    }

    public void setImpactoObjetivoDAO(ImpactoObjetivoDAO impactoObjetivoDAO) {
        this.impactoObjetivoDAO = impactoObjetivoDAO;
    }

    public List<ODS> getObjetivos() {
        return getOdsDAO().findAll();
    }

}
