/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author jpgm
 */
@MappedSuperclass  /* mapea esta clase como una super clase. Todas las clases que las herenden
                    van a tener id como primary key*/
public abstract class AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        } else if (!(obj instanceof AbstractEntity)) {
            return false;
        } else if (((AbstractEntity) obj).id.equals(this.id)) {
            return true;
        } else {
            return false;
        }
//        if (this == o) {
//            return true;
//        }
//        if (o == null
//                || !(o instanceof AbstractEntity)) {
//
//            return false;
//        }
//
//        AbstractEntity other = (AbstractEntity) o;
//
//        // if the id is missing, return false
//        if (id == null) {
//            return false;
//        }
//
//        // equivalence by id
//        return id.equals(other.getId());
    }

}
