package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.ActividadSimple;

import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "actividadSimpleConverter",forClass = ActividadSimple.class)
public class ActividadSimpleConverter extends AbstractConverter<ActividadSimple>{

    @Override
    public String getDAOName() {
        return "ActividadSimpleDAO";
    }
}
