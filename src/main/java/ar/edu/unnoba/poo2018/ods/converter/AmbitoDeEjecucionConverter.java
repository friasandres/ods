package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.AmbitoDeEjecucion;

import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "ambitoDeEjecucionConverter",forClass = AmbitoDeEjecucion.class)
public class AmbitoDeEjecucionConverter extends AbstractConverter<AmbitoDeEjecucion>{

    @Override
    public String getDAOName() {
        return "AmbitoDeEjecucionDAO";
    }
}
