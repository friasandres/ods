
package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ActividadDAO;
import ar.edu.unnoba.poo2018.ods.dao.UsuarioDAO;
import ar.edu.unnoba.poo2018.ods.model.Actividad;
import ar.edu.unnoba.poo2018.ods.model.Usuario;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@ManagedBean
@SessionScoped
public class SessionBacking implements Serializable {

    private String email;
    private String password;
    private Date desde = new Date();
    private Date hasta = new Date();
    private Usuario usuario = null;
    private List<Actividad> listaDelUsuario;
    private boolean estadoBotonTodas =  true;

    @EJB
    private UsuarioDAO usuarioDAO;
    @EJB
    private ActividadDAO actividadDAO;
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String login() {
        usuario = usuarioDAO.login(email, password);
        if (usuario == null) {
            return null;
        }
        verTodas();
        return "template?faces-redirect=true";
    }

    public String logout() {
        usuario = null;
        return "index.xhtml";
    }
    public boolean usuarioAutorizado(){
        if(getUsuario().isAdministrador()){
            return false;
        }else{
            return true;
        }
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }
    
    public void verTodas(){
        setListaDelUsuario(actividadDAO.actividadesUsuario(getUsuario()));
        setEstadoBotonTodas(true);
        setDesde(new Date());
        setHasta(new Date());
    }
    
    public void filtrarActividades(){
        setListaDelUsuario(actividadDAO.busqueda(getDesde(),getHasta(), getUsuario()));
        setEstadoBotonTodas(false);
        
    }

    public List<Actividad> getListaDelUsuario() {
        return listaDelUsuario;
    }

    public void setListaDelUsuario(List<Actividad> listaDelUsuario) {
        this.listaDelUsuario = listaDelUsuario;
    }

    public ActividadDAO getActividadDAO() {
        return actividadDAO;
    }

    public void setActividadDAO(ActividadDAO actividadDAO) {
        this.actividadDAO = actividadDAO;
    }

    public boolean isEstadoBotonTodas() {
        return estadoBotonTodas;
    }

    public void setEstadoBotonTodas(boolean estadoBotonTodas) {
        this.estadoBotonTodas = estadoBotonTodas;
    }
    
    
    
}
