package ar.edu.unnoba.poo2018.ods.dao;
import ar.edu.unnoba.poo2018.ods.model.LineaEstrategia;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

@Stateless
public class LineaEstrategiaDAO extends AbstractDAO<LineaEstrategia> {

    public LineaEstrategiaDAO() {
        super(LineaEstrategia.class);
    }

   
   
    public List<LineaEstrategia> findAll() {
        Query query = em.createNamedQuery("LineaEstrategia.findAll");
        return query.getResultList();
    }

}
