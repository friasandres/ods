package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.ActividadCompuesta;


import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "actividadCompuestaConverter",forClass = ActividadCompuesta.class)
public class ActividadCompuestaConverter extends AbstractConverter<ActividadCompuesta>{

    @Override
    public String getDAOName() {
        return "ActividadCompuestaDAO";
    }
}
