/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.unnoba.poo2018.ods.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Asus
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo")
@Table(name="actividad")
@NamedQueries({
    @NamedQuery(name = "Actividad.busqueda", query = "SELECT a FROM Actividad a INNER JOIN a.usuarios u WHERE a.fecha_inicio>= :desde AND a.fecha_fin<= :hasta AND u.mail =:mail"),
    @NamedQuery(name = "Actividad.actividadesUsuario", query = "SELECT a FROM Actividad a INNER JOIN a.usuarios u WHERE u.mail =:mail"),
    @NamedQuery(name = "Actividad.findAll", query = "SELECT u FROM Actividad u")
})
public abstract class Actividad extends AbstractEntity{
    @ManyToMany(mappedBy="actividades", cascade = CascadeType.ALL)
    private List<Usuario> usuarios;
    @Column(name="tipo")
    private String tipo;
    @Column(name="nombre")
    private String nombre;
    @Column(name="fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fecha_inicio;
    @Column(name="fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fecha_fin;
    @Column(name="resolucion")
    private String resolucion;
    @Column(name="expediente")
    private String expediente;
    @ManyToOne
    @JoinColumns(
        @JoinColumn(name="convocatoria_id",referencedColumnName = "id")
    )
    private Convocatoria convocatoria;
    @ManyToOne
    @JoinColumns(
        @JoinColumn(name="linea_estrategia_id",referencedColumnName = "id")
    )
    private LineaEstrategia lineaEstrategia;
    @ManyToOne
    @JoinColumns(
        @JoinColumn(name="ambito_id",referencedColumnName = "id")
    )
    private AmbitoDeEjecucion ambitoDeEjecucion;

    public Actividad() {
    }
    
    public abstract int getPeso();    
    public abstract double calcularPorcentaje();

    
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    
    
        
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public Convocatoria getConvocatoria() {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria) {
        this.convocatoria = convocatoria;
    }

    public LineaEstrategia getLineaEstrategia() {
        return lineaEstrategia;
    }

    public void setLineaEstrategia(LineaEstrategia lineaEstrategia) {
        this.lineaEstrategia = lineaEstrategia;
    }

    public AmbitoDeEjecucion getAmbitoDeEjecucion() {
        return ambitoDeEjecucion;
    }

    public void setAmbitoDeEjecucion(AmbitoDeEjecucion ambitoDeEjecucion) {
        this.ambitoDeEjecucion = ambitoDeEjecucion;
    }

    public String getTipo() {
        return tipo;
    }
    
    
    
    
}
