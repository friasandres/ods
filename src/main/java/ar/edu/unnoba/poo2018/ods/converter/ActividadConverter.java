package ar.edu.unnoba.poo2018.ods.converter;


import ar.edu.unnoba.poo2018.ods.model.ActividadSimple;

import javax.faces.convert.FacesConverter;


/**
 *
 * @author jpgm
 */
@FacesConverter(value = "actividadConverter",forClass = ActividadConverter.class)
public class ActividadConverter extends AbstractConverter<ActividadConverter>{

    @Override
    public String getDAOName() {
        return "ActividadDAO";
    }
}
