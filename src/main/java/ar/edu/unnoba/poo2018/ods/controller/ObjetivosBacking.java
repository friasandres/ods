package ar.edu.unnoba.poo2018.ods.controller;

import ar.edu.unnoba.poo2018.ods.dao.ImpactoObjetivoDAO;
import ar.edu.unnoba.poo2018.ods.dao.ODSDAO;
import ar.edu.unnoba.poo2018.ods.model.ImpactoObjetivo;
import ar.edu.unnoba.poo2018.ods.model.ODS;
import com.sun.javafx.image.impl.ByteArgb;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@ManagedBean(name = "objetivosBacking")
@ViewScoped
public class ObjetivosBacking implements Serializable {

    private ODS ods;
    private String objetivo; 
    private List<ODS> listaODS;
    private List<ImpactoObjetivo> impactoObjetivos;
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle");
    private int total;

    @EJB
    ODSDAO odsDAO;
    @EJB
    ImpactoObjetivoDAO impactoObjetivoDAO;
    @PostConstruct
    public void init() {
        this.ods = new ODS();
        this.objetivo = new String();
        this.total=0;
    }

    public String create() {
        FacesMessage message = null;

        try {

            getOds().setNombre(getObjetivo());

            getOdsDAO().create(getOds());

            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("objetivoCreado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/objetivos/index?faces-redirect=true";

        } catch (EJBException e) {
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/objetivos/index?faces-redirect=true";

        }

    }

    public String update() {
        FacesMessage message = null;

        try {

            odsDAO.update(this.getOds());
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("objetivoModificado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/objetivos/index?faces-redirect=true";
        } catch (EJBException e) {
          
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "/objetivos/index?faces-redirect=true";

        }
    }

    public String delete(ODS s) {
        FacesMessage message = null;

        try {

            getOdsDAO().delete(s);
            message = new FacesMessage(bundle.getString("Realizado"), bundle.getString("objetivoEliminado"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            return "/objetivos/index?faces-redirect=true";
        } catch (EJBException e) {
            message = new FacesMessage(bundle.getString("ocurrioError"), bundle.getString("accionNoRealizada"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
           
            return "/objetivos/index?faces-redirect=true";

        }
    }
    
    public List<ODS> lista(){
        setImpactoObjetivos(impactoObjetivoDAO.findAll());
        setListaODS(getOdsDAO().findAll());
        for(ODS o:getListaODS()){
            int total=0;
            for(ImpactoObjetivo i:getImpactoObjetivos()){
                if(i.getOds().getNombre().equals(o.getNombre())){
                    total+= i.getPeso();
                   
                }
            }
            this.total+=total;
            o.setPesoTotal(total);
            odsDAO.update(o);
        }
        return getListaODS();
    }

    public List<ODS> findAll() {
        return odsDAO.findAll();
    }
    public int totalPeso() {
        int result = impactoObjetivoDAO.pesoTotal();
        return result;
    }
    
    public HashMap<ODS,Double> totalObjetivo(){
        return impactoObjetivoDAO.totalObjetivo();
    }
    
    
    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public ODS getOds() {
        return ods;
    }

    public void setOds(ODS ods) {
        this.ods = ods;
    }

    public ODSDAO getOdsDAO() {
        return odsDAO;
    }

    public void setOdsDAO(ODSDAO odsDAO) {
        this.odsDAO = odsDAO;
    }

    public ResourceBundle getBundle() {
        return bundle;
    }

    public void setBundle(ResourceBundle bundle) {
        this.bundle = bundle;
    }

    public FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    public ImpactoObjetivoDAO getImpactoObjetivoDAO() {
        return impactoObjetivoDAO;
    }

    public void setImpactoObjetivoDAO(ImpactoObjetivoDAO impactoObjetivoDAO) {
        this.impactoObjetivoDAO = impactoObjetivoDAO;
    }

    public List<ODS> getListaODS() {
        return listaODS;
    }

    public void setListaODS(List<ODS> listaODS) {
        this.listaODS = listaODS;
    }

    public List<ImpactoObjetivo> getImpactoObjetivos() {
        return impactoObjetivos;
    }

    public void setImpactoObjetivos(List<ImpactoObjetivo> impactoObjetivos) {
        this.impactoObjetivos = impactoObjetivos;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
}
