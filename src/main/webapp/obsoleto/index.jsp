<%-- 
    Document   : newjsp
    Created on : 16/10/2018, 08:47:38
    Author     : Asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <script src="resources/js/Script.js" type="text/javascript"></script>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <form action="login" method="POST" onsubmit="return validar_login(this)">     
            <% if (response.getStatus() == response.SC_UNAUTHORIZED) { %>
            <label>Usuario/Contraseña incorrecta</label>
            <% }%>
            <label>Usuario:</label>
            <input type="email" name="mail"> 
            <label>Contraseña:</label>
            <input type="password" name="password"> 
            <input type="submit" value="Log in" id="loginbutton"> 
        </form>      

    </body>
</html>
